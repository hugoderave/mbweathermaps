#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.

# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

# Choix du serveur : NWS ou NCEP
# NB : sur le NWS, pas de GRIB 0.25°
#runurl="ftp://tgftp.nws.noaa.gov/SL.us008001/ST.opnl/"
runurl="ftp://ftp.ncep.noaa.gov/pub/data/nccf/com/gfs/prod/"

# Determine le run qu'on cherche a telecharger, en tenant compte d'un decalage dans le timing
runhour=$(date -d 'now -2 hours' +'%H' -u)
runday=$(date -d 'now -2 hours' +'%Y%m%d' -u)
runhour=$(echo "("$runhour"/6)*6" | bc)
#runhour=$(printf "%02d" $runhour)
runhour="00"

rundir="gfs."$runday$runhour"/"

# Dossier FTP : pour NCEP (1ere ligne), pour NWS (2eme ligne)
rundirftp=$rundir
#rundirftp="MT.gfs_CY."$runhour"/RD."$runday"/PT.grid_DF.gr2/"

runbasename="gfs.t"$runhour"z.pgrb2f"

# Pour les echeances >= 204 heures
runbasename2="gfs.t"$runhour"z.pgrbf27.grib2"  

basedir="$METEO_DIR/"
basedate=$(date -d 'now -2 hours' +'%Y-%m-%d' -u)
rundate=$(date -d 'now -2 hours' +'%d/%m/%Y' -u)

# Positionnement
cd $basedir 

# Nettoyage des runs precedents
rm -rf ./*.gr

# Nettoyage des liens du run courant
rm -f ./*.gr 

if [ -L fileinfo.txt ]; then
	rm -f fileinfo.txt
fi 

# Creation de la structure du run 
if [ ! -d $rundir ]; then
	mkdir $rundir
fi

rm -f $rundir"*.idx"
rm -f $rundir"log.txt"

if [ -f $rundir"fileinfo.txt" ]; then
	rm -f $rundir"fileinfo.txt"
fi

touch $rundir"fileinfo.txt"
touch $rundir"log.txt"

ln -s $basedir$rundir"fileinfo.txt" $basedir"fileinfo.txt"

# Boucle de telechargement
precfile=""
while IFS=";" read valid resolution trigger
do
	# Calcule la date correspondant au fichier telecharge
	calcul="$basedate $runhour:00 UTC +$valid hours"
	validdate=$(date -d "$calcul" +'%A %d %B %Y' | sed -e s/û/u/g | sed -e s/é/e/g )
	validhour=$(date -d "$calcul" +'%H')
	linkfile=$runbasename$valid".gr"

	# Boucle pour attendre le fichier sur le serveur distant
	pause=60
    downloaded=0
    while [ $pause -le 30770 ]
    do
		# Obtention du fichier index
		zz="000"$valid

		# fichier pour le serveur NWS
		#fmt=$(echo $zz | rev | cut -c 1-4 | rev )
		#runfile="fh."$fmt"_tl.press_gr.0p5deg"
		
		# fichier pour le serveur NCEP (+choix de la resolution)
		fmt=$(echo $zz | rev | cut -c 1-3 | rev )
		runfile="gfs.t"$runhour"z.pgrb2."$resolution".f"$fmt
		#runfile="gfs.t"$runhour"z.pgrb2.0p25.f"$fmt

		# La presence du fichier IDX indique que le GRIB est dispo
		if curl -f -s -m 180 --retry 2 $runurl$rundirftp$runfile".idx" -o $basedir$rundir$runbasename$valid".idx" ; then
	
		    # Utiliser la ligne ci-dessous pour un telechargement partiel des seuls champs utilises
			# Nb : pose parfois des pb de reprise en cas de coupure du téléchargement
		    #cat $basedir$rundir$runbasename$valid".idx" | ./calc_ranges.pl | egrep "$(./make_inv.pl)" | ./get_grib.pl $runurl$rundir$runbasename$valid $basedir$rundir$runbasename$valid 

			# Utiliser la ligne ci-dessous pour obtenir le fichier entier
                    if curl -f -s -m 1200 --retry 10 $runurl$rundirftp$runfile -o $basedir$rundir$runbasename$valid ; then
                        #wget -o log.txt --no-verbose -nc $runurl$rundir$runbasename$valid

			# Unification du nom
			ln -s $basedir$rundir$runbasename$valid $basedir$linkfile

			# Sortie pour le fileinfo et parallel
			out_info=$linkfile";GFS;"$runhour";"$rundate";"$valid";"$validdate";"$validhour":00;"$precfile
			echo $out_info >> fileinfo.txt
			# Pas de sortie parallel pour l'analyse
			if [ $valid -ne "00" ]
			then
				echo $out_info
			fi
			precfile=$linkfile
                        downloaded=1
                        pause=30770
                    else
                        echo "Download error $runurl$rundirftp$runfile.idx" $basedir$rundir$runbasename$valid".idx" 1>&2
                        sleep $pause
                    fi
		else
                    echo "File not ready $runurl$rundirftp$runfile.idx" $basedir$rundir$runbasename$valid".idx" 1>&2
                    sleep $pause
		fi
		# On multiplie le temps d'attente par 2 jusqu'a 10 minutes
		pause=$(($pause*2))
	done

    if [ $downloaded -eq 0 ] 
	then
		echo "Aborting "$runurl$rundir$runbasename$valid".idx" $basedir$rundir$runbasename$valid".idx" 1>&2
	fi

	if ! [ -z "$trigger" ]; then
		$trigger | batch 1>&2
	fi
done < runlist.txt




