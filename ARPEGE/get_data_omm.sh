#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.

# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

#PATH=/home/nicolas/Meteo/bin:$PATH
# Notre serveur de donnees
runurl="https://donneespubliques.meteofrance.fr/inspire/services/ArpegeWFS?SERVICE=WFS&VERSION=2.0.0&REQUEST=GetFeature&STOREDQUERYID=urn:fr.meteofrance:def:query:OGC-WFS::GetGridObservation&DATASETID=urn:xwmo:md:fr-meteofrance::inspire-wmores40-grib-mg-mf-glob25&CRS=EPSG:4326&TOKEN=__agnS45LVlkEKvdqZEV3YweFJaMWoNldD__"
ftpuser="your_inspire_username"
ftppass="jour_inspire_api_key"
getkey="https://donneespubliques.meteofrance.fr/inspire/services/GetAPIKey?username=$ftpuser&passwordftppass"

# Determine le run qu'on cherche a telecharger, qui est generalement dispo 8 heures apres
runhour=$(date -d 'now -2 hours' +'%H')
runday=$(date -d 'now -2 hours' +'%Y%m%d')
runhour=$(echo "("$runhour"/12)*12" | bc)
runhour=$(printf "%02d" $runhour)

rundir="arpege."$runday$runhour"0000/"

basedir="$METEO_DIR/"
basedate=$(date -d 'now -2 hours' +'%Y-%m-%d')
rundate=$(date -d 'now -2 hours' +'%d/%m/%Y')

# Positionnement
cd $basedir

# Nettoyage des runs precedents
rm -rf ./arpege.*

# Nettoyage des liens du run courant
rm -f ./*.nc 

if [ -L fileinfo.txt ]; then
	rm -f fileinfo.txt
fi 

# Creation de la structure du run 
if [ ! -d $rundir ]; then
	mkdir $rundir
fi

rm -f $rundir"*.bin"
rm -f $rundir"log.txt"

if [ -f $rundir"fileinfo.txt" ]; then
	rm -f $rundir"fileinfo.txt"
fi

touch $rundir"log.txt"

ln -s $basedir$rundir"fileinfo.txt" $basedir"fileinfo.txt"

# Choppe tous les fichiers d'un coup
cd $rundir

prev_valid="-1"
while IFS=";" read valid variable level
do
	# Le run 12h ne va pas jusqu'à +96h
	if [ "$runhour$valid" != "1296" ] 
	then
		# Calcule la date correspondant au fichier telecharge
		calcul="$basedate $runhour:00 UTC +$valid hours"
		validdate=$(date -d "$calcul" +'%A %d %B %Y' | sed -e s/û/u/g | sed -e s/é/e/g )
		validhour=$(date -d "$calcul" +'%H')
		timedl=$(date -d "$calcul" +'%Y-%m-%dT%H' -u)":00:00Z"
		file=$valid"_"$variable"_"$level
		linkfile=$file".nc"
	
		if [ -z "$level" ] 
		then
			wget -O $file $runurl"&REFERENCE_TIME="$basedate"T"$runhour":00:00Z&TIME="$timedl"&OBSERVED_PROPERTY="$variable --no-verbose
		else
			wget -O $file $runurl"&REFERENCE_TIME="$basedate"T"$runhour":00:00Z&TIME="$timedl"&VERTICAL_CRS=ISOBARIC&ELEVATION="$level"&OBSERVED_PROPERTY="$variable --no-verbose  
		fi

		munpack $file
		mv part1 $file".gr"
		../bin/grib_to_netcdf	-o $linkfile $file".gr"

		ln -s $basedir$rundir$linkfile $basedir$linkfile

		if [ "$valid" != "$prev_valid" ] 
		then
			out_info=$valid";ARPEGE;"$runhour";"$rundate";"$valid";"$validdate";"$validhour":00;"
			echo $out_info >>  $basedir"fileinfo.txt"
			echo $out_info
		fi
	fi
	prev_valid=$valid
done < ../runlist.txt
