# README #

Ce document indique à quoi sert le projet MB Weather Maps et comment le mettre 
en oeuvre.

### A quoi sert MB Weather Maps ?###

MB Weather Maps est un outil développé à l'origine pour le blog et compte Twitter
Météo Blois, utilisé depuis 2012 pour réaliser des prévisions météo.

Il a pour but de générer des cartes de prévision pour la France et l'Europe à 
partir de données de modèles en open data. 

La solution est composée d'un ensemble de scripts Bash et NCL qui remplissent 
les fonctions suivantes :

- obtenir les données à partir des centres météo

- faire calculer le modèle à maille fine WRF-NMM sur la France

- générer des cartes et diagrammes

Des cartes prêtes pour la production sont disponibles pour les modèles 
suivants :

- GFS (en résolution 0.5° mais paramétrable pour 0.25°)

- CFS (cartes daily)

- ECMWF

- ARPEGE (données WMO)

Un support très limité et expérimental est fourni pour les modèles suivants :

- AROME

- ARPEGE 

MB Weather Maps est distribué sous licence GNU GPL (voir le fichier gpl.txt).

### Configuration requise ###

Les scripts sont prévus pour fonctionner sur un système Linux 64 bits. 

Ils ont été testés et utilisés sur la distribution Ubuntu 14.04 LTS, mais 
devraient pouvoir s'adapter sur d'autres versions et distributions.

La configuration matérielle minimale recommandée est la suivante :

- Processeur x86 64bits 4 coeurs 2.5Ghz (type core i5 ou i7)

- 16 Go de RAM

- 120 Go de disque dur

Ce readme suppose que l'utilisateur est familier d'un certain nombres d'outils 
et de pratiques systèmes (ex : savoir installer des paquets, savoir compiler des
logiciels, savoir utiliser git, bash, cron...). Aussi ajoutons la configuration 
suivante :

- Un administrateur système ayant de sérieuses connaissances en Unix/Linux et 
quelques connaissances en développement ;)

### Pré-requis logiciels ###

Les outils système suivants sont requis :

- atd (batch, at...) pour la mise en file d'attente de scripts

- parallel pour l'exploitation des threads

- bash pour l'essentiel des scripts

- korn shell et C shell (ksh, csh) pour certains scripts (UPP et WRF)

- GNU GhostScript pour convertir les cartes en postscript vers PNG

- ImageMagick retraitement des cartes PNG 

- Open MPI pour lancer WRF sur plusieurs coeurs

- curl et wget pour les téléchargements de fichiers


Les outils suivants sont requis et supposés installés dans le path selon la 
procédure en vigeur indiquée sur les sites d'origine :

- NCL (https://www.ncl.ucar.edu/) pour la génération des cartes météo

- ECMWF GRIB API (https://software.ecmwf.int/wiki/display/GRIB/Home) pour 
certains retraitements (ARPEGE WMO)

Le modèle WRF et son post-processeur doivent être compilés manuellement au 
préalable. Cette partie étant complexe, la section suivante vous indiquera 
comment le déployer.
Note : ne sont pas mentionnés ici les pré-requis pour compiler WRF.  Les
éléments suivants sont supposés compilés par l'utilisateur et fonctionnels :

- WRF (http://www2.mmm.ucar.edu/wrf/users/)

- UPP (https://dtcenter.org/upp/users/)

Des données supplémentaires peuvent être nécessaires pour dessiner les contours 
de départements (format shapefile). Leur licence n'étant pas identique, ils 
ne peuvent être inclus dans ce package. Vous pouvez en obtenir ici : 
https://www.data.gouv.fr/fr/datasets/contours-des-departements-francais-issus-d-openstreetmap/
Vous devrez probablement adapter les chemins dans les scripts NCL.

### Comment mettre en oeuvre ###

Déploiement et configuration :

- Premièrement installer les pré-requis logiciels.

- Compiler WRF. Il est conseillé de suivre ce tutoriel : 
http://www.meteo-blois.fr/mise-en-oeuvre-des-modeles-meteo-wrf-arw-et-wrf-nmm-partie-1-introduction/
Dans ce qui suit, on supposera que vous l'avez lu et mis en oeuvre, et êtes 
maintenant familier avec le fonctionnement du modèle.

- Cloner le repository (git clone) ou copier les sources dans un dossier, 
appelons-le METEO_DIR.

- METEO_DIR/WRF/nmm_real : copier les exécutables wrf.exe, real_nmm.exe, 
ungrib.exe, metgrid.exe, geogrid.exe préalablement compilés. Copier également 
le contenu du dossier test/nmm_real de votre source WRF.

- METEO_DIR/UPPV2.1 : doit contenir l'environnement et l'exécutable unipost.

- Editer le fichier scripts/mb_config.ncl et modifier le chemin pour mettre 
votre METEO_DIR.

- Définissez la variable d'environnement NCARG_ROOT vers votre installation de 
NCL. Exportez cette variable depuis votre .bashrc ou autre méthode selon votre 
installation.

Fonctionnement. Les scripts suivants sont à planifier selon les besoins :

- gfs_run.sh : récupère les données du dernier GFS et génère les cartes à 
mesure.

- ecmwwf_run.sh : récupère le dernier ECMWF et génère les cartes.

- cfs_run.sh : récupère le dernier CFS et génère les cartes.

- wrf_run.sh : calcule WRF à partir des données du dernier GFS, et génère les 
cartes.

- gfs_run_trigger.sh : génère des diagrammes GFS à la fin de l'obtention des 
données

Paramétrage :

- GFS/runlist.txt : éditez le fichier pour choisir les échéances et la 
résolution souhaitée (0p25 ou 0p50). La troisième colonne peut être utiliséer 
pour lancer un script à la fin de la récupération de l'échéance indiquée 
(par exemple déclencher WRF quand les 48h de données sont obtenues)

- WRF/nmm_real/nmm_run.sh : modifier les variables des namelist dans ce script 
pour modifier ce que vous souhaitez (ex : le nombre d'heures à simuler...)

- ARPEGE/get_data_omm.sh : modifiez vos identifiants API Key

- Vous pouvez modifier la quantité de coeurs utilisée par les scripts en 
modifiant les appels à la commande Parallel en éditant les scripts. Idem
avec les appels à mpirun dans les scripts WRF.

Fichiers produits :

- Les cartes et diagrammes sont générées dans le dossier scripts/images 
(classées en fr, europe, lat, lon, skewt). Les images des précédents runs sont 
remplacées par le nouveau run, mais ne sont pas supprimées.

- Les données modèles sont enregistrées dans le dossier du modèle. Les données 
précédentes sont automatiquement supprimées au début de chaque run.

### Contribution ###

Ce projet est fourni "tel quel", en espérant qu'il soit utile à quelqu'un. 
Il est le résultat d'un besoin pour le site Meteo Blois et n'est 
assurément pas parfait. Soyez libres de l'améliorer et de nous en faire 
profiter ! Il y aurait tant à faire :

- écrire un support complet pour AROME et ARPEGE. Le support est aujourd'hui 
balbutiant.

- rendre configurable les thèmes de couleurs qui ont été empruntés ici et là, 
voire improvisés.

- améliorer le scripting pour rendre cela plus paramétrable, et corriger les 
quelques problèmes actuels.

- et tant d'autres choses !

N'hésitez pas à contacter l'auteur pour proposer vos modifications.

### Support ###

Si vous avez besoin d'aide ou avez des questions, veuillez les poser
de préférence dans les commentaires sur les articles dédiés du blog Meteo Blois 
(http://www.meteo-blois.fr/tutoriels/).

Vous pouvez également contacter directement l'auteur.

### A propos de l'auteur ###

L'auteur de MB Weather Maps est Nicolas Gasnier. 

Il tient le blog Météo Blois (http://www.meteo-blois.fr/) et anime le 
compte Twitter https://twitter.com/MeteoBlois.

Contact email : ngasnier chez orange.fr
