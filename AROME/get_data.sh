#!/bin/bash

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.

# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

#PATH=/home/nicolas/Meteo/bin:$PATH

# Notre serveur de donnees
runurl="http://dcpc-nwp.meteo.fr/services/PS_GetCache_DCPCPreviNum?token=__5yLVTdr-sGeHoPitnFc7TZ6MhBcJxuSsoZp6y0leVHU__&model=AROME&grid=0.025&format=grib2"

# Determine le run qu'on cherche a telecharger, qui est generalement dispo 8 heures apres
runhour=$(date -d 'now -5 hours' +'%H')
runday=$(date -d 'now -5 hours' +'%Y%m%d')
runhour=$(echo "("$runhour"/6)*6" | bc)
runhour=$(printf "%02d" $runhour)

rundir="arome."$runday$runhour"0000/"

basedir="$METEO_DIR/"
basedate=$(date -d 'now -2 hours' +'%Y-%m-%d')
rundate=$(date -d 'now -2 hours' +'%d/%m/%Y')

# Positionnement
cd $METEO_DIR

# Nettoyage des runs precedents
rm -rf ./arome.*

# Nettoyage des liens du run courant
rm -f ./*.grib2

if [ -L fileinfo.txt ]; then
	rm -f fileinfo.txt
fi 

# Creation de la structure du run 
if [ ! -d $rundir ]; then
	mkdir $rundir
fi

rm -f $rundir"*.bin"
rm -f $rundir"log.txt"

if [ -f $rundir"fileinfo.txt" ]; then
	rm -f $rundir"fileinfo.txt"
fi

touch $rundir"log.txt"

ln -s $basedir$rundir"fileinfo.txt" $basedir"fileinfo.txt"

# Choppe tous les fichiers d'un coup
cd $rundir

prev_valid="-1"
prev_file=""
while IFS=";" read valid package level
do
	# Calcule la date correspondant au fichier telecharge
	file="AROME_0.025_"$valid"_"$package".grib2"
	linkfile=$file

#"package=IP1&time=00H06H&referencetime=2015-07-22T03:00:00Z"
#	wget -O $file $runurl"&referencetime="$basedate"T"$runhour":00:00Z&TIME="$valid"&package="$package --no-verbose

	curl -f -s -m 600 --retry 10 $runurl"&referencetime="$basedate"T"$runhour":00:00Z&TIME="$valid"&package="$package -o $basedir$rundir$file

	ln -s $basedir$rundir$linkfile $basedir$linkfile

	if [ "$valid" != "$prev_valid" ] 
	then
		start="0"${valid:0:2}
		end="0"${valid:3:2}
		for hour in `seq $start $end`
		do
			calcul="$basedate $runhour:00 UTC +$hour hours"
			validdate=$(date -d "$calcul" +'%A %d %B %Y' | sed -e s/û/u/g | sed -e s/é/e/g )
			validhour=$(date -d "$calcul" +'%H')

			hfmt="0"$hour
			hfmt=${hfmt:(-2)}
			out_info=$valid";AROME;"$runhour";"$rundate";"$hfmt";"$validdate";"$validhour":00;"$prev_file
			echo $out_info >>  $basedir"fileinfo.txt"
			echo $out_info
		done
		if [ "$prev_valid" != "-1" ]
		then
			prev_file=$prev_valid
		else 
			prev_file=$valid
		fi
	fi
	prev_valid=$valid
done < ../runlist.txt
