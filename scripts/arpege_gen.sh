#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.

# Author : Nicolas GASNIER

export PATH=/home/nicolas/Meteo/ncar/bin:$PATH
export NCARG_ROOT=/home/nicolas/Meteo/ncar

cd /home/nicolas/Meteo/scripts

ncl arpege_all.ncl

#cat << EOF | parallel -n 4 ncl arpege_all.ncl
#p_start=0
#p_end=12
#p_start=13
#p_end=24
#p_start=25
#p_end=36
#p_start=37
#p_end=48
#EOF

#p_start=49
#p_end=60
#p_start=61
#p_end=72
#p_start=73
#p_end=84
#p_start=85
#p_end=96


for f in $(ls images/eur/arpege_*.ps)
do
	outfile="images/eur/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r72 -sOutputFile=$outfile -c "<</Orientation 1>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done

for f in $(ls images/fr/arpege_*.ps)
do
	outfile="images/fr/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r102 -sOutputFile=$outfile -c "<</Orientation 0>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done
#done



