#!/bin/sh
# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.
#
# Author : Nicolas Gasnier
#
rot=$1
dpi=$2
f=$3
outfile="$(dirname $f)/$(basename $f .ps).png"
# png256
# Conversion PS en PNG
gs -sDEVICE=png16m -dNOPAUSE -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r$dpi -sOutputFile=$outfile -c "<</Orientation $rot>> setpagedevice" --f $f quit 
# Trim les bandes blanches inutiles
convert $outfile -trim +repage $outfile
composite -geometry +1+49 masque_depts_noir.png $outfile $outfile 
rm -f $f


