#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.
#
# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

cd $METEO_DIR

# Les 48 heures d'un run WRF sur 4 threads
cat << EOF | parallel -n 2 ncl wrf_all_nest_2.ncl
p_start=0
p_end=5
p_start=6
p_end=11
p_start=12
p_end=17
p_start=18
p_end=23
p_start=24
p_end=29
p_start=30
p_end=35
p_start=36
p_end=41
p_start=42
p_end=47
EOF

ls images/fr/wrf_*.ps | parallel ./ps2png_fr.sh 0 102

