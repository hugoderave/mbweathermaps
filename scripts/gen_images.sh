#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.
#
# Author : Nicolas GASNIER

for f in $(ls images/eur/*.ps)
do
	outfile="images/eur/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r72 -sOutputFile=$outfile -c "<</Orientation 1>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done

for f in $(ls images/fr/*.ps)
do
	outfile="images/fr/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 0>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done

for f in $(ls images/skewt/*.ps)
do
	outfile="images/skewt/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 0>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done

for f in $(ls images/lon/*.ps)
do
	outfile="images/lon/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 1>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done
