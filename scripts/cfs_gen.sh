#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.

# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

cd $METEO_DIR

# Pour le moment on se contente de 384 heures
cat << EOF | parallel -n 2 ncl cfs_all.ncl
p_start=0
p_end=15
p_start=16
p_end=31
p_start=32 
p_end=47
p_start=48 
p_end=63
EOF

ls images/eur/cfs_*.ps | parallel ./ps2png.sh 1 72
ls images/fr/cfs_*.ps | parallel ./ps2png.sh 0 102

