#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.
#
# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

cd $METEO_DIR
echo $METEO_DIR

fileinfo=$1
echo $1
#while read fileinfo 
#do
runvalid=$(echo $fileinfo | cut -d ";" -f 5)

cmdline="p_fileinfo=\""$fileinfo"\""
ncl gfs_all.ncl "$cmdline"

for f in $(ls images/eur/gfs_*_$runvalid.ps)
do
	outfile="images/eur/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r72 -sOutputFile=$outfile -c "<</Orientation 1>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim +repage $outfile
	rm -f $f
done

for f in $(ls images/fr/gfs_*_$runvalid.ps)
do
	outfile="images/fr/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r102 -sOutputFile=$outfile -c "<</Orientation 0>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim +repage $outfile
	composite -geometry +1+49 masque_depts_noir.png $outfile $outfile 
	rm -f $f
done

for f in $(ls images/skewt/*_$runvalid.ps)
do
	outfile="images/skewt/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 0>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim +repage $outfile
	rm -f $f
done

for f in $(ls images/lon/*_$runvalid.ps)
do
	outfile="images/lon/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 1>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done

for f in $(ls images/lat/*_$runvalid.ps)
do
	outfile="images/lat/$(basename $f .ps).png"
	# png256
	# Conversion PS en PNG
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 1>> setpagedevice" --f $f quit 
	# Trim les bandes blanches inutiles
	convert $outfile -trim $outfile
	rm -f $f
done
#done



