#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.
#
# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

cd $METEO_DIR

cmdline="p_mode=\"3J\""
ncl gfs_meteogram.ncl "$cmdline" &

cmdline="p_mode=\"7J\""
ncl gfs_meteogram.ncl "$cmdline"


for f in $(ls images/*.ps)
do
	outfile="images/$(basename $f .ps).png"
	gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 0>> setpagedevice" --f $f quit 
	convert $outfile -trim +repage $outfile
	rm -f $f
done

#f="images/gfs_meteo_3J.ps"
#outfile="images/gfs_meteo_3J.png"
#gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</#Orientation 0>> setpagedevice" --f $f quit 
#convert $outfile -trim +repage $outfile
#rm -f $f

#f="images/gfs_meteo_7J.ps"
#outfile="images/gfs_meteo_7J.png"
#gs -sDEVICE=png16m -dNOPAUSE -dEPSCrop -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r90 -sOutputFile=$outfile -c "<</Orientation 0>> setpagedevice" --f $f quit 
#convert $outfile -trim +repage $outfile
#rm -f $f

