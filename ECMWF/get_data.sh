#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.

# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

# Notre serveur de donnees
runurl="ftp://data-portal.ecmwf.int/"
ftpuser="wmo"
ftppass="essential"

# Determine le run qu'on cherche a telecharger, qui est generalement dispo 8 heures apres
runhour=$(date -d 'now -8 hours' +'%H')
runday=$(date -d 'now -8 hours' +'%Y%m%d')
runhour=$(echo "("$runhour"/12)*12" | bc)
runhour=$(printf "%02d" $runhour)

rundir=$runday$runhour"0000/"

basedir="$METEO_DIR/"
basedate=$(date -d 'now -8 hours' +'%Y-%m-%d')
rundate=$(date -d 'now -8 hours' +'%d/%m/%Y')

# Positionnement
cd $basedir

# Nettoyage des runs precedents
rm -rf ./*.gr

# Nettoyage des liens du run courant
rm -f ./*.gr 

if [ -L fileinfo.txt ]; then
	rm -f fileinfo.txt
fi 

# Creation de la structure du run 
if [ ! -d $rundir ]; then
	mkdir $rundir
fi

rm -f $rundir"*.bin"
rm -f $rundir"log.txt"

if [ -f $rundir"fileinfo.txt" ]; then
	rm -f $rundir"fileinfo.txt"
fi

touch $rundir"log.txt"

ln -s $basedir$rundir"fileinfo.txt" $basedir"fileinfo.txt"

# Choppe tous les fichiers d'un coup
cd $rundir
wget -o log.txt --no-verbose --no-parent --recursive --level=1 --no-directories --user=$ftpuser --password=$ftppass $runurl$rundir*.bin 

# Cree des liens avec un nom plus sympatoche...
for f in $(ls *.bin)
do
	subst="s/.*"$runday$runhour"0000_//"
	linkfile=$(echo $f | sed -e "$subst")
	ln -s $basedir$rundir$f $basedir$linkfile".gr"
done

# On cree le fileinfo
cd ..
for f in $(ls *.gr | grep -o "[0-9]\{1,3\}h_" | sed -e "s/_//" | sed -e :a -e 's/^.\{1,3\}$/0&/;ta' | sort | uniq | sed -e "s/^0//")
do
	valid=$(echo $f | sed -e "s/h//")
	calcul="$basedate $runhour:00 UTC +$valid hours"
	validdate=$(date -d "$calcul" +'%A %d %B %Y' | sed -e s/û/u/g | sed -e s/é/e/g )
	validhour=$(date -d "$calcul" +'%H')

	out_info=$f";ECMWF;"$runhour";"$rundate";"$valid";"$validdate";"$validhour":00;"$precfile
	echo $out_info >>  $basedir"fileinfo.txt"
	echo $out_info
done

