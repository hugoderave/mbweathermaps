#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.

# Author : Nicolas GASNIER
    
METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

# Notre serveur de donnees
runurl="http://www.ftp.ncep.noaa.gov/data/nccf/com/cfs/prod/cfs/"


# Determine le run qu'on cherche a telecharger, qui est generalement dispo 8 heures apres
runhour=$(date -d 'now -18 hours' +'%H')
runday=$(date -d 'now -18 hours' +'%Y%m%d')
runhour=$(echo "("$runhour"/12)*12" | bc)
runhour=$(printf "%02d" $runhour)

rundir="cfs."$runday"/"$runhour"/"

basedir="$METEO_DIR/"
basedate=$(date -d 'now -18 hours' +'%Y-%m-%d')
rundate=$(date -d 'now -18 hours' +'%d/%m/%Y')

# Positionnement
cd $basedir

# Nettoyage des runs precedents
rm -rf ./*.grb2

# Nettoyage des liens du run courant
rm -f ./*.grb2

if [ -L fileinfo.txt ]; then
	rm -f fileinfo.txt
fi 

# Creation de la structure du run 
if [ ! -d $rundir ]; then
	mkdir -p $rundir
fi

rm -f $rundir"*.bin"
rm -f $rundir"log.txt"

if [ -f $rundir"fileinfo.txt" ]; then
	rm -f $rundir"fileinfo.txt"
fi

touch $rundir"log.txt"

ln -s $basedir$rundir"fileinfo.txt" $basedir"fileinfo.txt"

# Choppe tous les fichiers qui nous interessent
cd $rundir
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/tmp2m.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/t1000.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/t925.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/t850.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/t700.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/t250.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/t200.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/tmin.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/tmax.04."$runday$runhour".daily.grb2"

wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/z1000.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/z850.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/z700.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/z500.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/z200.04."$runday$runhour".daily.grb2"

wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/prmsl.04."$runday$runhour".daily.grb2"

wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/prate.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/cprat.04."$runday$runhour".daily.grb2"

wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/tcdcclm.04."$runday$runhour".daily.grb2"

wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/wnd10m.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/wnd1000.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/wnd925.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/wnd850.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/wnd700.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/wnd250.04."$runday$runhour".daily.grb2"
wget -o log.txt --no-verbose $runurl$rundir"time_grib_04/wnd200.04."$runday$runhour".daily.grb2"

# Cree les liens
for f in $(ls *.grb2)
do
	ln -s $basedir$rundir$f $basedir$f
done

# On cree le fileinfo
cd ..
calcul="$basedate $runhour:00 UTC"
validdate=$(date -d "$calcul" +'%d/%m/%Y' | sed -e s/û/u/g | sed -e s/é/e/g )
validhour=$(date -d "$calcul" +'%H:%M')
calcul="$basedate $runhour:00 UTC +2784 hours"
validdateend=$(date -d "$calcul" +'%d/%m/%Y' | sed -e s/û/u/g | sed -e s/é/e/g )
validhourend=$(date -d "$calcul" +'%H:%M')
out_info=$runday$runhour";CFS;"$runhour";"$rundate";06;"$validdate";"$validhour";;2784;"$validdateend";"$validhourend";6"
echo $out_info >>  $basedir"fileinfo.txt"
echo $out_info

