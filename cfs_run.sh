#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.
#
# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi

# Attend un peu que la connexion réseau soit Ok...
sleep 30

# Supprime les anciens runs
cd "$METEO_DIR/CFS/"
for dir in cfs.*
do
	if [ -d $dir ]; then
		rm -rf $dir
	fi
done

# Telechargement et generation des cartes
cd "$METEO_DIR/CFS/"
./get_data.sh
cd "$METEO_DIR/scripts/"
./cfs_gen.sh
