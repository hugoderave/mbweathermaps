#!/bin/sh

# This file is part of MB Weather Maps.
#
# Foobar is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Foobar is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with MB Weather Maps.  If not, see <http://www.gnu.org/licenses/>.
#
# Author : Nicolas GASNIER

METEO_DIR=$( cd "$(dirname "$0")" ; pwd -P )
if [ ! -d "$METEO_DIR" ]; then METEO_DIR="$PWD"; fi
METEO_DIR=$(realpath $METEO_DIR"/../../")

cd $METEO_DIR/WRF/nmm_real

# Nettoie les fichiers de l'ancien run
rm -f FILE\:*
rm -f GRIBFILE.*
rm -f met_nmm.*
rm -f wrfout_*
rm -f ../../GFS/wrf/*

# Link les fichiers GFS requis
gfs_files=$(cat ../../GFS/fileinfo.txt | head -n 17 | cut -d ";" -f 1)
for file in $gfs_files
do
	ln -s ../../GFS/$file ../../GFS/wrf/$file
done

./link_grib.csh ../../GFS/wrf/

# Variables nécessaires
dt=$(cat ../../GFS/fileinfo.txt | head -n 1 | cut -d ";" -f 4)
gfs_start_day=$(echo $dt | cut -d "/" -f 1)
gfs_start_month=$(echo $dt | cut -d "/" -f 2)
gfs_start_year=$(echo $dt | cut -d "/" -f 3)
gfs_start_date=$gfs_start_year-$gfs_start_month-$gfs_start_day
gfs_start_hour=$(cat ../../GFS/fileinfo.txt | head -n 1 | cut -d ";" -f 3)
gfs_start=$gfs_start_date"_"$gfs_start_hour":00:00"

dt=$(cat ../../GFS/fileinfo.txt | head -n 13 | tail -n 1 | cut -d ";" -f 4)
gfs_end_date=$(date -d "$gfs_start_date $gfs_start_hour:00:00 UTC +48 hours" +'%Y-%m-%d' -u)
gfs_end_day=$(echo $gfs_end_date | cut -d "-" -f 3)
gfs_end_month=$(echo $gfs_end_date | cut -d "-" -f 2)
gfs_end_year=$(echo $gfs_end_date | cut -d "-" -f 1)
gfs_end_time=$(date -d "$gfs_start_date $gfs_start_hour:00:00 UTC +48 hours" +'%H:%M:%S' -u)
gfs_end_hour=$(echo $gfs_end_time | cut -d ":" -f 1)
gfs_end=$gfs_end_date"_"$gfs_end_time

# Prépare le namelist.wps
cat << EOF > $METEO_DIR/WRF/nmm_real/namelist.wps
&share
 wrf_core = 'NMM',
 max_dom = 1,
 start_date = '$gfs_start',
 end_date = '$gfs_end',
 interval_seconds = 10800
 io_form_geogrid = 2,
 debug_level = 1000,
/

&geogrid
 parent_id         =   1,
 parent_grid_ratio =   1,
 i_parent_start    =   1,
 j_parent_start    =   1,
 e_we              =  128,
 e_sn              =  256,
 geog_data_res     = '2m',
 dx = 0.05, 
 dy = 0.05, 
 map_proj = 'lambert',
 ref_lat   =  46.80, 
 ref_lon   =  2.3372, 
 truelat1  =  45.8989,
 truelat2  =  47.6960,
 stand_lon = 2.3372, 
 geog_data_path = '$METEO_DIR/geog'
/

&ungrib
 out_format = 'WPS',
 prefix = 'FILE',
/

&metgrid
 fg_name = 'FILE'
 io_form_metgrid = 2, 
/
EOF

# Prépare le namelist.input
cat << EOF2 > $METEO_DIR/WRF/nmm_real/namelist.input
&time_control
 run_days                            = 0,
 run_hours                           = 48,
 run_minutes                         = 0,
 run_seconds                         = 0,
 start_year                          = $gfs_start_year,
 start_month                         = $gfs_start_month,
 start_day                           = $gfs_start_day,
 start_hour                          = $gfs_start_hour,
 start_minute                        = 00,
 start_second                        = 00,
 end_year                            = $gfs_end_year,
 end_month                           = $gfs_end_month,
 end_day                             = $gfs_end_day,
 end_hour                            = $gfs_end_hour,
 end_minute                          = 00,
 end_second                          = 00,
 interval_seconds                    = 10800
 input_from_file                     = .true.,
 history_interval                    = 60,
 frames_per_outfile                  = 1000,
 restart                             = .false.,
 restart_interval                    = 10800,
 io_form_history                     = 2
 io_form_restart                     = 2
 io_form_input                       = 2
 io_form_boundary                    = 2
 debug_level                         = 1000
 /

 &domains
 time_step                           = 15,
 time_step_fract_num                 = 0,
 time_step_fract_den                 = 1,
 max_dom                             = 1,
 e_we                                = 128,
 e_sn                                = 256,
 e_vert                              = 30,
 p_top_requested                     = 5000,
 num_metgrid_levels                  = 27,
 num_metgrid_soil_levels             = 4,
 dx                                  = 0.05,
 dy                                  = 0.05,
 grid_id                             = 1,
 parent_id                           = 0,
 i_parent_start                      = 1,
 j_parent_start                      = 1,
 parent_grid_ratio                   = 1,
 parent_time_step_ratio              = 1,
 feedback                            = 1,
 smooth_option                       = 0
 /

 &physics
 mp_physics                          = 5,
 ra_lw_physics                       = 99,
 ra_sw_physics                       = 99,
 nrads                               = 105,
 nradl                               = 105,
 co2tf                               = 1,
 sf_sfclay_physics                   = 2,
 sf_surface_physics                  = 2,
 bl_pbl_physics                      = 2,
 nphs                                = 6,
 cu_physics                          = 1,
 ncnvc                               = 6,
 tprec                               = 3,
 theat                               = 6,
 tclod                               = 6,
 trdsw                               = 6,
 trdlw                               = 6,
 tsrfc                               = 6,
 pcpflg                              = .false.,
 num_soil_layers                     = 4,
 mp_zero_out                         = 0,
 gwd_opt                             = 0
 /

&noah_mp
 dveg                                = 2,
 opt_crs                             = 1,
 opt_sfc                             = 1, 
 opt_btr                             = 1,
 opt_run                             = 1,
 opt_frz                             = 1,
 opt_inf                             = 1,
 opt_rad                             = 1,
 opt_alb                             = 2,
 opt_snf                             = 1,
 opt_tbot                            = 2,
 opt_stc                             = 1,
/

 &fdda
 /

 &dynamics
 dyn_opt                             = 4
 non_hydrostatic                     = .true.,
 euler_adv                           = .true.,
 idtadt                              = 1,
 idtadc                              = 1,
 codamp                              = 6.4,
 coac                                = 1.6,
 slophc                              = 6.364e-3,
 wp                                  = 0.15,
 /

 &bdy_control
 spec_bdy_width                      = 5,
 spec_zone                           = 1,
 relax_zone                          = 4,
 specified                           = .true.,
 nested                              = .false.,
 /

 &grib2
 /

 &namelist_quilt
 nio_tasks_per_group = 0,
 nio_groups = 1,
 /

&dfi_control
 dfi_opt                             = 0,
 dfi_nfilter                         = 7,

/
EOF2

# Etapes du preprocesseur
vi na./geogrid.exe
./ungrib.exe
./metgrid.exe

# Etapes du run
mpirun -np 4 --bind-to-core ./real_nmm.exe
mpirun -np 4 --bind-to-core ./wrf.exe

# Etapes du postprocesseur
./run_unipost

